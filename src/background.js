import { run as getSpectranetDetails, ERROR } from './functions/getSpectranetDetails'
import wrapRetry from './functions/wrapRetry'
import { getStorage, setStorage } from './functions/storage'
import changeBadgeText from './functions/changeBadgeText';

setInterval(function () {

  getStorage(async (storage) => {

    if (storage['loggedIn']) {
      const response = await wrapRetry(3, getSpectranetDetails)(storage['username'], storage['password'])
      console.log(response)
      if (response.error !== undefined) return  
      // else if (response.error === undefined){
        if ('dataRemaining' in response) {
          changeBadgeText(response.dataRemaining)
          let temp = storage
          temp.profileDetails.dataRemaining = response.dataRemaining
          temp.profileDetails.dataTotal = response.dataTotal
          temp.profileDetails.daysRemaining = response.daysRemaining
          temp.profileDetails.percentage = response.percentage
          setStorage(temp, null)
        }
      }
    // }
  })
}, 300000)
