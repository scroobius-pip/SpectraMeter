import React from 'react'
import styles from '../../global.style'
import {Progress} from 'react-sweet-progress-simdi'
import 'react-sweet-progress-simdi/lib/style.css'

// import injectSheet from 'react-jss'

const circularProgress = ({percent, styleProps}) => {
  return (<Progress
    theme={{
      active: {
        color: styles.colors.primary,
        outlineColor: styles.colors.white
      },
      default: {
        color: styles.colors.white
      },
      success: {
        color: styles.colors.primary
      }
    }}
    width={240}
    style={{
      circle: {
        fontFamily: styles.fonts.family,
        color: styles.colors.white,
        boxShadow: styles.shadow,
        borderRadius: '100%',
        // marginLeft: '30px',
        // marginBottom: '25px'
        ...styleProps
      },
      value: {
        position: 'relative',
        bottom: '10px',
        marginRight: '5px',
        fontSize: '54px',
        fontWeight: styles.fonts.weight.semibold

      },
      percent: {
        fontSize: '22px',
        position: 'relative',
        top: '2px',
        fontWeight: styles.fonts.weight.regular
      },
      label: {
        fontSize: '20px',
        top: '40px',

        fontWeight: styles.fonts.weight.regular

      }
    }}
    label='Remaining'
    type='circle'
    percent={percent >= 100 ? 99 : percent} />)
}

export default circularProgress
