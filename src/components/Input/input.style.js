import globalStyles from '../../global.style'
const {colors, fonts} = globalStyles

export default {

  'main': {
    // float: 'left',
    width: '65%',
    // margin: '40px 3%',
    position: 'relative',
    margin: '0 auto',
    paddingBottom: '40px'
  },
  'input': {
    font: `15px/24px ${fonts.family}`,
    color: colors.white,
    width: '100%',
    boxSizing: 'border-box',
    letterSpacing: '1px',
    border: 0,
    padding: '7px 0',
    background: 'transparent',
    borderBottom: `1.3px solid ${colors.primary}`,
    '&:focus': {
      outline: 'none'
    },
    '&::-webkit-input-placeholder': {
      color: colors.white,
      fontWeight: fonts.weight.semilight,
      fontSize: '19px',
      letterSpacing: '-0.5px'
    }

  },
  'border': {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: 0,
    height: '2px',
    backgroundColor: colors.primary,
    transition: '0.4s'
  }
//   '.effect-1:focus ~ .focus-border': {
//     width: '100%',
//     transition: '0.4s'
//   }
}
