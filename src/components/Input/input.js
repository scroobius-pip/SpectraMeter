import React from 'react'
import styles from './input.style'
import injectSheet from 'react-jss'

const input = ({classes, password, placeholder, value, name, onChange}) => {
  return (
    <div className={classes.main}>
      <input name={name} onChange={onChange} value={value} className={classes.input} type={password ? 'password' : 'text'} placeholder={placeholder} />
      <span className={classes.border} />
    </div>
  )
}

export default injectSheet(styles)(input)
