import React from 'react'
import styles from './refreshButton.style'
import injectSheet from 'react-jss'
import refresh from './refresh.svg'

const refreshButton = ({classes, onClick, refreshing, style}) => {
  return (

    <div style={style} onClick={onClick} className={!refreshing ? classes.main : classes.refreshing}>
      <img alt='' src={refresh} />
    </div>
  )
}

export default injectSheet(styles)(refreshButton)
