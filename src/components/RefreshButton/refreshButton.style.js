export default {
  main: {
    width: '17px',
    height: '17px',
    cursor: 'pointer'
  },
  refreshing: {
    width: '17px',
    height: '17px',
    cursor: 'pointer',
    animation: 'rotation 2s infinite linear'

  },
  '@keyframes rotation': {
    'from': {
      transform: 'rotate(0deg)',
      transformOrigin: '50% 50%'
    },

    'to': {
      'transform': 'rotate(359deg)',
      transformOrigin: '50% 50%'
    }
  }
}
