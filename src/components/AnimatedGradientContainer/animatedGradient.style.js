import globalStyles from '../../global.style'

const {colors} = globalStyles
export default {
  main: {
    width: '100%',
    height: '100%',
    background: `linear-gradient(174deg, ${colors.primary}, ${colors.secondary})`,
    backgroundSize: '300% 300%',
    animation: 'Gradient 5s ease infinite'
  },
  '@keyframes Gradient': {
    '0%': {
      backgroundPosition: '0% 74%'
    },
    '50%': {
      backgroundPosition: '100% 27%'
    },
    '100%': {
      backgroundPosition: '0% 74%'
    }
  }
}
