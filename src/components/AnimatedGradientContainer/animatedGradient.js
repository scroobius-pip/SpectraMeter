import React from 'react'
import styles from './animatedGradient.style'
import injectSheet from 'react-jss'

const animatedGradient = ({classes, children, style}) => {
  return (
    <div style={style} className={classes.main} >
      {children}
    </div>
  )
}

export default injectSheet(styles)(animatedGradient)
