import globalStyles from '../../global.style'
const {colors, fonts} = globalStyles

export default {
  main: {
    display: 'block',
    color: colors.white,
    fontFamily: fonts.family,
    margin: '0 auto',
    fontSize: '14px'

  },
  gb: {
    fontWeight: fonts.weight.semibold
  },
  tr: {
    '&>*': {
      paddingLeft: '2px',
      fontWeight: fonts.weight.bold
    }
  },
  line: {
    borderRight: `6px solid ${colors.white}`
  }
}
