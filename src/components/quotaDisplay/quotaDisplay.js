import React from 'react'
// import {main, start} from './roundButton.css'
import styles from './quotaDisplay.style'
import injectSheet from 'react-jss'
// import RefreshButton from '../RefreshButton'
const quotaDisplay = ({classes, remaining, total, onClick, styleProps, Component}) => {
  return (
    <table style={styleProps} className={classes.main}>
      <tr className={classes.tr} >
        {Component ? <td> <Component /></td> : null}
        <td>{remaining}</td>
        <td className={classes.gb}>GB</td>
        <td className={classes.line} />

        <td>{total}</td>
        <td className={classes.gb}>GB</td>
      </tr>

    </table>

  )
}

export default injectSheet(styles)(quotaDisplay)
