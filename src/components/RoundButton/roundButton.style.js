import globalStyles from '../../global.style'
const {colors, fonts} = globalStyles

export default {
  main: {
    display: 'block',
    '&>*': {
      margin: '0px',
      padding: '0px',
      boxSizing: 'border-box',
      color: colors.primary,
      fontFamily: fonts.family,
      fontSize: '17px',
      fontWeight: fonts.weight.bold,
      width: '100%'
    }
  },
  start: {
    background: colors.white,
    borderRadius: '25px',
    border: 'none',
    width: '180px',
    height: '45px',
    display: 'inline-block',
    textAlign: 'center',
    boxShadow: '0 5px 30px rgba(0, 0, 0, 0.12), 0 1px 10px rgba(0, 0, 0, 0.2)',
    '&:active': {
      boxShadow: '0 5px 30px rgba(0, 0, 0, 0.12), 0 0px 0px rgba(0, 0, 0, 0.24)'
    },
    '&:focus': {
      outline: 0
    }
  },
  loader: {
    height: '45px',
    width: '45px',
    backgroundColor: 'white',
    borderRadius: '50%',
    display: 'table-cell',
    verticalAlign: 'middle',
    textAlign: 'center',
    animation: 'rotation 2s infinite linear',
    marginLeft: 'auto',
    marginRight: 'auto',
    left: 0,
    right: 0

  },
  '@keyframes rotation': {
    'from': {
      transform: 'rotate(0deg)'
    },

    'to': {
      'transform': 'rotate(359deg)'
    }
  },
  'loaderImg': {
    position: 'relative',
    top: '5px'
  },
  container: {
    position: 'relative',
    margin: '0 auto',
    width: '50%',
    textAlign: 'center',
    '&>*': {
      position: 'absolute'
    }
  }
}
