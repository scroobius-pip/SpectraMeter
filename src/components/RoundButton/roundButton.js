import React from 'react'
// import {main, start} from './roundButton.css'
import styles from './roundButton.style'
import injectSheet from 'react-jss'
import loader from './loader.svg'

const Loader = ({classes}) => (
  <div className={classes.loader}>
    <img alt=''
      className={classes.img}
      src={loader} />
  </div>
)

const roundButton = ({classes, text, onClick, loading}) => {
  return (
    <div className={classes.container}>
      {!loading ? (
        <div className={classes.main}>
          <button onClick={onClick} className={classes.start}>{text}</button>
        </div>
  ) : (<Loader classes={{loader: classes.loader, img: classes.loaderImg}} />)}
    </div>)
}

export default injectSheet(styles)(roundButton)
