import React from 'react'
import styles from '../../global.style'
import {Progress} from 'react-sweet-progress-simdi'
import 'react-sweet-progress-simdi/lib/style.css'
import './lineProgress.css'

const LineProgress = ({password, percent}) => {
  return (<Progress
    theme={{
      active: {
        color: styles.colors.primary,
        outlineColor: styles.colors.white
      },
      default: {
        color: styles.colors.white
      },
      success: {
        color: styles.colors.primary
      }
    }}
    width={250}
    style={lineStyles.lineProgress}
    label='Remaining'
    percent={percent || 0} />)
}

const DaysRemaining = ({days}) => {
  return (
    <div style={lineStyles.daysRemaining.container}>
      <span style={lineStyles.daysRemaining.days}>{days}</span>
      <span style={lineStyles.daysRemaining.remaining}>days remaining</span>
    </div>
  )
}

const Wrapper = ({percent, days, styleProps}) => {
  return (
    <div style={lineStyles.wrapper, styleProps}>
      <DaysRemaining days={days} />
      <LineProgress percent={percent} />
    </div>
  )
}

const lineStyles = {
  'daysRemaining': {
    container: {
      color: styles.colors.white,
      marginBottom: '20px',
      marginLeft: '36px'
    },
    days: {
      fontSize: '48px',
      fontWeight: styles.fonts.weight.semibold,
      marginRight: '5px',
      fontFamily: styles.fonts.family
    },
    remaining: {
      fontSize: '25px',
      fontWeight: '600',
      fontFamily: styles.fonts.family
    }

  },
  'wrapper': {
    fontFamily: styles.fonts.family,
    width: '250px',
    margin: '0 auto'
    // marginLeft: '20px'
  },
  'lineProgress': {
    width: '240px',
    boxShadow: styles.shadow,
    margin: '0 auto',
    borderRadius: '100%'

  }
}

export default Wrapper
