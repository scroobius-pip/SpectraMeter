import React from 'react'
// import {main, start} from './roundButton.css'
import styles from './textButton.style'
import injectSheet from 'react-jss'

const textButton = ({classes, text, onClick}) => {
  return (
    <button onClick={onClick} className={classes.text}> {text} </button>

  )
}

export default injectSheet(styles)(textButton)
