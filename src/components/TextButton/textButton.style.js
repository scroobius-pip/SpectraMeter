import globalStyles from '../../global.style'
const {colors, fonts} = globalStyles

export default {
  text: {
    background: 'none',
    border: 'none',
    margin: 0,
    padding: 0,
    outline: 'none',
    color: colors.white,
    fontFamily: fonts.family,
    fontSize: 17.5,
    fontWeight: fonts.weight.regular,
    cursor: 'pointer'
  }
}
