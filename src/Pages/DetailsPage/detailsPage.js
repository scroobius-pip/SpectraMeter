import React, {Component} from 'react'
import {
  RefreshButton,
  AnimatedGradientContainer,
  CircularProgress,
  LineProgress,
  QuotaDisplay,
  TextButton
} from '../../components'
import injectSheet from 'react-jss'
import logo from './logo.svg'
import styles from './detailsPage.style'
import {connect} from 'react-redux'
import logout from '../../reducers/logOut.action'
import Spinner from 'react-spinner'
import 'react-spinner/react-spinner.css'
import setDetails from '../../reducers/setProfileDetails.action'
import {run as getSpectranetDetails, ERROR} from '../../functions/getSpectranetDetails'
import wrapRetry from '../../functions/wrapRetry'
import changeBadgeText from '../../functions/changeBadgeText'

const Logo = ({classes}) => (<img src={logo} className={classes.logo} />)

class detailsPage extends Component {
  constructor () {
    super()
    this.state = {
      refreshing: false
    }
  }

  logout () {
    this
      .props
      .logout()
  }

  async onRefreshClicked () {
    this.setState({'refreshing': true})
    const { setDetails, data: {username, password} } = this.props

    const response = await wrapRetry(3, getSpectranetDetails)(username, password)
    if (response.error && response.error === ERROR.AUTH) {
      console.log('Auth Error')
    } else if (response.error && response.error === ERROR.REQUEST) {
      // dispatchSomethingHere()

    } else if ('dataRemaining' in response) {
      console.log('response:' + typeof response)
      console.log(response)
      changeBadgeText(response.dataRemaining)
      if (response) { setDetails(response) }
    }
    this.setState({'refreshing': false})
    // const {}
  }

  render () {
    const {
      classes,
      data: {
        dataRemaining,
        dataTotal,
        daysRemaining,
        loginId,
        percentage
      }

    } = this.props
    return (

      <div className={classes.page}>
        <AnimatedGradientContainer style={{
          padding: 20
        }}>
          <div className={classes.topBar}>
            <span className={classes.id}>{loginId}</span>

            <Logo classes={classes} />
          </div>
          <CircularProgress
            styleProps={{
              marginBottom: '25px'
            }}
            className={classes.circular}
            percent={percentage} />

          <QuotaDisplay
            Component={() => <RefreshButton onClick={() => this.onRefreshClicked()} refreshing={this.state.refreshing} />}
            styleProps={{
              marginLeft: '168px'
            }}
            total={dataTotal}
            remaining={dataRemaining} />
          <LineProgress
            styleProps={{
              marginBottom: '25px'
            }}
            percent={(daysRemaining / 30 * 100)}
            days={daysRemaining} />
          <TextButton onClick={() => this.logout()} text='Log out' />
        </AnimatedGradientContainer>
      </div>

    )
  }
}
const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logout()),
    setDetails: (details) => dispatch(setDetails(details))
  }
}

const mapStateToProps = state => {
  const {profileDetails: {dataRemaining, dataTotal, daysRemaining, loginId, percentage}, username, password} = state
  return {
    data: {
      dataRemaining,
      dataTotal,
      daysRemaining,
      loginId,
      percentage,
      username,
      password
    }
  }
}

const detailsPageStyled = injectSheet(styles)(detailsPage)

export default connect(mapStateToProps, mapDispatchToProps)(detailsPageStyled)
