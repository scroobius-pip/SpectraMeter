import globalStyles from '../../global.style'
import mainStyles from '../main.style'

const {colors, fonts} = globalStyles
const {page} = mainStyles
export default {
  page,
  topBar: {
    display: 'flex',
    justifyContent: 'space-between',
    height: '45px'
  },
  id: {
    fontFamily: fonts.family,
    color: colors.white,
    fontWeight: fonts.weight.regular,
    fontSize: '14px'
  },
  logo: {
    height: '90%'

  },
  circular: {
    marginBottom: '25px'
  }
}
