import React, {Component} from 'react'
import {AnimatedGradientContainer, Input, RoundButton} from '../../components'
import styles from './loginPage.style'
import injectSheet from 'react-jss'
import {run as getSpectranetDetails, ERROR} from '../../functions/getSpectranetDetails'
import {connect} from 'react-redux'
import login from '../../reducers/login.action'
import setPassword from '../../reducers/setPassword.action'
import setDetails from '../../reducers/setProfileDetails.action'
import setUsername from '../../reducers/setUsername.action'
import wrapRetry from '../../functions/wrapRetry'
import logo from './logo.svg'
import changeBadgeText from '../../functions/changeBadgeText'

const Logo = ({classes}) => (
  <img src={logo} className={classes.logo} />
)

class loginPage extends Component {
  constructor () {
    super()
    this.state = {
      username: '',
      password: '',
      loading: false,
      invalidAuth: false
    }
  }

  componentDidMount () {
    const {username, password} = this.props
    this.setState({username, password})
  }

  handleInputChange (e) {
const {setPassword,setUsername}  = this.props
switch(e.target.name){
case 'username':
      setUsername(e.target.value)
      break
case 'password':
      setPassword(e.target.value)
      break
default:
      break
}
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  async onKeyPress () {
    this.setState({'loading': true, 'invalidAuth': false})
    const {login, setDetails, setPassword, setUsername} = this.props
    const {username, password} = this.state
    const response = await wrapRetry(3, getSpectranetDetails)(username, password)
    if (response.error && response.error === ERROR.AUTH) {
      this.setState({'invalidAuth': true})
    } else if (response.error && response.error === ERROR.REQUEST) {
      // dispatchSomethingHere()

    } else {
      console.log('logging in')
      changeBadgeText(response.dataRemaining)
      setDetails(response)
      setPassword(password)
      setUsername(username)
      login()
    }
    this.setState({'loading': false})
  }
  handleEnterPressed (e) {
    if (e.charCode === 13) {
      this.onKeyPress()
    }
  }

  

  render () {
    const { classes } = this.props
    return (

      <div tabIndex='1' onKeyPress={e => this.handleEnterPressed(e)} className={classes.page}>
        <AnimatedGradientContainer style={{padding: 20}}>
          <Logo classes={{logo: classes.logo}} />
          <p className={classes.welcome}>Welcome</p>
          <p className={classes.enter}>Enter your spectranet details</p>
          <Input name='username' value={this.state.username} onChange={this.handleInputChange.bind(this)} placeholder='User ID' />
          <Input name='password' value={this.state.password} onChange={this.handleInputChange.bind(this)} placeholder='Password' password />
          <RoundButton onClick={() => this.onKeyPress()} loading={this.state.loading} text='Login' />
          {this.state.invalidAuth ? <p className={classes.invalid}>Invalid Username or Password</p> : null}
        </AnimatedGradientContainer>
      </div>

    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    login: () => dispatch(login()),
    setUsername: (username) => dispatch(setUsername(username)),
    setPassword: (password) => dispatch(setPassword(password)),
    setDetails: (details) => dispatch(setDetails(details))
  }
}

const mapStateToProps = state => {
  // console.log(state)
  const {username, password} = state
  return {username, password}
}

const loginPageStyled = injectSheet(styles)(loginPage)
export default connect(mapStateToProps, mapDispatchToProps)(loginPageStyled)
