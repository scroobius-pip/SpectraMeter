import globalStyles from '../../global.style'
import mainStyles from '../main.style'

const {colors, fonts} = globalStyles
const {page} = mainStyles
export default {
  page,
  logo: {
    width: '18%',
    marginBottom: '25px'
  },
  welcome: {
    fontFamily: fonts.family,
    fontSize: '26px',
    color: colors.white,
    fontWeight: fonts.weight.semibold,
    marginBottom: '25px'

  },
  enter: {
    fontFamily: fonts.family,
    fontSize: '15px',
    fontWeight: fonts.weight.semilight,
    color: colors.white,
    marginBottom: '35px'

  },
  invalid: {
    fontFamily: fonts.family,
    fontSize: '15px',
    fontWeight: fonts.weight.semilight,
    color: colors.white,
    marginTop: '70px'
  }
}
