
var request = require('request-promise-native')

const ERROR = {
  AUTH: 'auth',
  REQUEST: 'request'
}  // refactor this as an external file

async function run (username, password) {
  if (typeof username === 'undefined' || typeof password === 'undefined') { return {error: ERROR.AUTH} }

  var options = {
    method: 'POST',
    url: 'https://wt-45a59387d15d83e94c65e12b483c8b00-0.sandbox.auth0-extend.com/SpectraMe' +
        'ter-dev-main/',
    headers: {
      'content-type': 'application/json'
    },
    body: {
      username: username,
      password: password,
      retries: 3
    },
    json: true
  }

  try {
    let response = await request(options)
    return response
  } catch (err) {
  }
  // return getDetails(response)
}

export {run, ERROR}
