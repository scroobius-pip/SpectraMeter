/* global chrome*/

export default (value) => {
    const remaining = parseFloat(value);
    const color = remaining < 2 ? 'red' : 'green'
    const text = remaining < 1 ? 'low': `${Math.round(remaining)} GB`
    chrome.browserAction.setBadgeBackgroundColor({color})
    chrome.browserAction.setBadgeText({ text })
}