const retry = require('./async-retry')

const ERROR = {
  AUTH: 'auth',
  REQUEST: 'request'
}

export default function wrapRetry (max, func) {
  return async (...args) => {
    return retry(async (bail, retries) => {
      console.log('retrying ' + retries)
      let response = await func(...args)
      console.log('from wrap retry' + JSON.stringify(response))
      if (response.error && response.error === ERROR.REQUEST) {
        throw response
      } else {
        return response
      }
    }, {retries: max}).catch(err => {
      return err
    })
  }
}
