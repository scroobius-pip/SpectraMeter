/* global chrome */

function setStorage (object, callback) {
  try {
    chrome.storage.local.set(object, callback)
  } catch (err) {

  }
}
async function getStorage (callback) {
  try {
    await chrome.storage.local.get(result => {
      callback(result)
    })
  } catch (err) {
    callback()
  }
}

export {
    setStorage,
    getStorage

}
