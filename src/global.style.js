 export default {
   colors: {
     primary: '#FF901D',
     secondary: '#D83981',
     white: '#FFFFFF'
   },
   fonts: {
     family: 'Lato',
     weight: {
       light: '100',
       semilight: '300',
       semibold: '700',
       regular: '400',
       bold: '900'
     }
   },
   shadow: '0 5px 30px rgba(0, 0, 0, 0.12), 0 1px 10px rgba(0, 0, 0, 0.2)'

 }
