import constants from './constants'

export default () => {
  return {
    type: constants.LOGOUT
  }
}
