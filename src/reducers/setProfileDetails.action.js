import constants from './constants'

export default (details) => {
  // console.log('i am in profile details' + JSON.stringify(details))
  return {
    type: constants.SET_PROFILE_DETAILS,
    payload: details
  }
}
