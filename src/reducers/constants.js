export default {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
  // SET_COOKIE: 'SET_COOKIE',
  // SET_URLTOKEN: 'SET_URLTOKEN',
  SET_USERNAME: 'SET_USERNAME',
  SET_PASSWORD: 'SET_PASSWORD',
  SET_PROFILE_DETAILS: 'SET_PROFILE_DETAILS'
}
