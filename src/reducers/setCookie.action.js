import constants from './constants'

export default (cookie) => {
  return {
    type: constants.SET_COOKIE,
    payload: cookie
  }
}
