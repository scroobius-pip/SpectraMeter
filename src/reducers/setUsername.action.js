import constants from './constants'

export default (username) => {
  return {
    type: constants.SET_USERNAME,
    payload: username
  }
}
