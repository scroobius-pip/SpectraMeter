// import {LOGIN, LOGOUT, SET_PASSWORD, SET_USERNAME, SET_URLTOKEN, SET_COOKIE}
// from './constants'
import constants from './constants'
const initialState = {
  username: '',
  loggedIn: false,
  password: '',
  cookie: '',
  profileDetails: {
    dataRemaining: '',
    dataTotal: '',
    daysRemaining: '',
    loginId: '',
    percentage: ''
  }

}

const rootReducer = (state = initialState, action) => {
  const {type, payload} = action
  switch (type) {
    case constants.LOGIN:
      return {
        ...state,
        loggedIn: true
      }
    case constants.LOGOUT:
      return {
        ...state,
        loggedIn: false,
        username: '',
        password: ''
      }
    case constants.SET_PASSWORD:
      return {
        ...state,
        password: payload
      }
    case constants.SET_USERNAME:
      return {
        ...state,
        username: payload
      }
    case constants.SET_COOKIE:
      return {
        ...state,
        cookie: payload
      }
    case constants.SET_PROFILE_DETAILS:
      // console.log('i am in reducer' + JSON.stringify(action.payload))
      return {
        ...state,
        profileDetails: {
          ...action.payload
        }
      }
      // case constants.SET_URLTOKEN:   return ''
    default:
      return state
  }
}

export default rootReducer
