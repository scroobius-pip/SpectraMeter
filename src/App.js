import React, { Component } from 'react'
import './App.css'
import { LoginPage, DetailsPage } from './pages'
import { connect } from 'react-redux'

class App extends Component {
  constructor() {
    super()

    this.state = {

    }
  }

  render() {
    return (

      <div className='App'>

        {this.props.isLoggedIn
          ? <DetailsPage />
          : <LoginPage />
        }
      </div>

    )
  }
}
const mapStateToProps = state => {
  return { isLoggedIn: state.loggedIn }
}

export default connect(mapStateToProps)(App)
