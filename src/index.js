/* global chrome */
/* global alert */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import 'typeface-lato';
import App from './App';
import { getStorage, setStorage } from './functions/storage';
import './index.css';
import rootReducer from './reducers';


getStorage((result) => {
  const store = createStore(rootReducer, result)

  store.subscribe(async () => {
    setStorage(store.getState(), null)
    getStorage((result) => {
      console.log(`From storage ${JSON.stringify(result)}`)
    })
  })

  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>, document.getElementById('root'))
})

