// var express = require('express')
// var Webtask = require('webtask-tools')
// var bodyParser = require('body-parser')
const cheerio = require('cheerio')
const request = require('request-promise-native')
const retry = require('async-retry')
// var app = express()

// app.use(bodyParser.urlencoded({ extended: true }))
// app.use(bodyParser.json())

// app.use(bodyParser.urlencoded({ extended: true }));

// app.post('/', async function (req, res) {
//   console.log(req.body)
//   const {username, password, retries} = req.body

//   const response = await run(username, password, retries)
//   res.json(response)
// })

const ERROR = {
  AUTH: 'auth',
  REQUEST: 'request'
}

function wrapRetry (max, func) {
  return async (...args) => {
    return retry(async (bail, retries) => {
      console.log('retrying ' + retries)
      let response = await func(...args)
      // console.log(response)
      if (response.error && response.error === ERROR.REQUEST) {
        throw response
      } else {
        return response
      }
    }, {retries: max}).catch(err => {
      return err
    })
  }
}

async function getSpectranetLoginToken () {
  const options = {
    method: 'GET',
    uri: 'https://selfcare.spectranet.com.ng',
    resolveWithFullResponse: true
    // mode: 'no-cors',
    // credentials: 'include'
  }
  try {
    const response = await request(options)
    // const response = await fetch('https://selfcare.spectranet.com.ng', options)
    // console.log(response)
    // console.log(response.headers)
    let cookie = response.headers['set-cookie'].join(';')
    console.log(cookie)
    const $ = cheerio.load(response.body)
    let str = $('form').attr('action')
    str = str.replace('../../../', '')
    str = str.split(';')
    str = str[0]
    return [str, cookie]
    // return [str, null]
  } catch (error) {
    return { error: ERROR.REQUEST, message: error, from: 'getSpectranetLoginToken' }
  }
}

const appendToken = str => 'https://selfcare.spectranet.com.ng/alepowsrc/' + str

const getDetails = body => {
  let padToThree = number => number <= 999 ? ('00' + number).slice(-3) : number

  // const $ = cheerio.load(body)
  console.log('trying getDetails')
  try {
    // const dataRemainingGbMbKb = $('table.dataTable>tbody>tr:nth-child(1)>td:nth-child(2)>label').text().split(' ')[0].split(':')
    const dataRemainingGbMbKb = ['26', '44', '533']
    // const dataTotal = $('.speakout').eq(8).text().split(',')[0].split('=')[1].replace('GB', '')
    // const loginId = $('.speakout').eq(0).text()
    // const daysRemaining = $('.infobox>span').eq(0).text()
    const [dataRemainingGb, dataRemainingMb] = dataRemainingGbMbKb
    const dataRemaining = `${dataRemainingGb || ''}.${padToThree(dataRemainingMb)}`
    return {
      dataRemaining: (Math.round(parseFloat(dataRemaining) * 10) / 10)
      // dataTotal,
      // daysRemaining,
      // loginId,
      // percentage: (Math.round(parseFloat(dataRemaining / dataTotal * 100) * 10) / 10)
    }
  } catch (err) {
    if (passwordCorrect(body)) {
      return {error: ERROR.REQUEST, message: err, from: 'getDetails'}
    }
    return {error: ERROR.AUTH, message: err, from: 'getDetails'}
  }
}

const passwordCorrect = body => {
  // console.log(body.indexOf('Invalid Username or Password'))
  // console.log(body)
  return !(body.indexOf('Invalid Username or Password') !== -1)
}

const getStoredCookies = () => {
  return ''
}

const getStoredUrlToken = () => {
  return ''
}

const setStoredCookies = str => {
  // return true
  return ''
}

const setStoredUrlToken = str => {
  return true
}
const clearStoredCookies = () => {}
const clearStoredUrlToken = () => {}

async function getSpectranetProfile (url, cookie, username, password) {
  console.log('trying getSpectranetProfile')

  const options = {
    formData: {
      'signInForm.password': password,
      'signInForm.username': username,
      'signInContainer:submit': 'Sign In',
      'id44_hf_0': ''
    },
    method: 'POST',
    uri: url,
    qs: {
      '': ''
    },
    headers: {
      'content-type': 'multipart/form-data; boundary=---011000010111000001101001',
      cookie: cookie
    }

  }
  try {
    const body = await request(options)
    return body
  } catch (error) {
    return {error: ERROR.REQUEST, message: error, from: 'getSpectranetProfile'}
  }
}

async function run (username, password, retries) {
  if (typeof username === 'undefined' || typeof password === 'undefined') return {error: ERROR.AUTH}
  let urlToken = getStoredUrlToken()
  let cookie = getStoredCookies()

  if (!cookie || !urlToken) {
    const response = await wrapRetry(retries, getSpectranetLoginToken)()

    if (response.error) { return response }
    [urlToken, cookie] = response
    console.log(urlToken, cookie)
    setStoredCookies(cookie)
    setStoredUrlToken(urlToken)
  }

  async function getSpectranetProfileDetails () {
    let response = await getSpectranetProfile(appendToken(urlToken), cookie, username, password)
    if (response.error) { return response }
    return getDetails(response)
  }

  const response = await wrapRetry(retries, getSpectranetProfileDetails)()

  if (response.error) {
    clearStoredUrlToken()
    clearStoredCookies()
    return response
  }
  return response
}

module.exports = {
  run,
  getDetails
}
